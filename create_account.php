<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>

<h2>Create Account</h2>

<form action="home_page.html" method="post">
  <div class="imgcontainer">
    <img src="palm_tree.jpg" height="200" width="200" alt="Oasis">
  </div>

  <div class="container">
    <label for="surname"><b>Enter Name</b></label>
    <input type="text" placeholder="Enter Name" name="surname" required>
    
    <label for="uname"><b>Enter User Name</b></label>
    <input type="text" placeholder="Enter User Name" name="uname" required>

    <label for="uemail"><b>User email</b></label>
    <input type="text" placeholder="Enter email" name="uemail" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>

    <label for="cpsw"><b>Confirm Password</b></label>
    <input type="password" placeholder="Confirm Password" name="cpsw" required>

    <button type="submit" name="submit1">Create Account</button>
    
  </div>

  <div class="container" style="background-color:#fff3e6">
    <button type="button" onclick="location.href = 'home_page.html';" class="cancelbtn">Cancel</button>
  </div>
  
</form>



</body>
</html>
